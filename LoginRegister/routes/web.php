<?php 


Route::group(['middleware' => ['web']],function(){
    Route::get('register','Custome\Auth\Http\Controllers\CustomeAuth@index')->name('register');
    Route::post('register','Custome\Auth\Http\Controllers\CustomeAuth@submit');
    Route::get('login','Custome\Auth\Http\Controllers\CustomeAuth@login')->name('login');
    Route::post('login','Custome\Auth\Http\Controllers\CustomeAuth@SubmitLogin'); 
    Route::get('logout','Custome\Auth\Http\Controllers\CustomeAuth@logout'); 

});

?> 