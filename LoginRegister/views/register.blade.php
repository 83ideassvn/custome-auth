<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
	<title>Registration</title>
</head>

<body>
	<div class="container"> @if(\Session::has('success'))
		<div class="alert alert-success" role="alert"> {{ \Session::get('success') }} </div> @endif @if(isset($errors)) @if ($errors->any()) @foreach ($errors->all() as $error)
		<div class="alert alert-danger" role="alert"> {{ $error }} </div> @endforeach @endif @endif
		<form action="{{route('register')}}" method="post"> @csrf
			<h5 class="text-center">Create New Account</h5>
			<div class="row ">
				<div class="col-md-3"> </div>
				<div class="col-md-6 jumbotron">
					<div class="col-sm-12 form-group">
						<label for="name-f"> Name</label>
						<input type="text" class="form-control" name="name" id="name" placeholder="Enter your name." > 
                      
                    </div>
					<div class="col-sm-12 form-group">
						<label for="email">Email</label>
						<input type="email" class="form-control" name="email" id="email" placeholder="Enter your email." > </div>
					<div class="col-sm-12 form-group">
						<label for="pass">Password</label>
						<input type="Password" name="password" class="form-control" id="pass" placeholder="Enter your password." > </div>
					<div class="col-sm-12 form-group mb-0">
						<button class="btn btn-primary float-right">Submit</button>
					</div>
				</div>
				<div class="col-md-3"> </div>
			</div>
		</form>
	</div>
</body>

</html>