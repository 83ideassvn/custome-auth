<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
	<title>Login</title>
</head>

<body>
	<div class="container">
		<form action="{{route('login')}}" method="post"> @csrf
			<h5 class="text-center">Login Your Account</h5>
			<div class="row ">
				<div class="col-md-3"> </div>
				<div class="col-md-6 jumbotron">
					<div class="col-sm-12 form-group">
						<label for="email">Email</label>
						<input type="email" class="form-control" name="email" id="email" placeholder="Enter your email." required> </div>
					<div class="col-sm-12 form-group">
						<label for="pass">Password</label>
						<input type="Password" name="password" class="form-control" id="pass" placeholder="Enter your password." required> </div>
					<div class="col-sm-12 form-group mb-0">
						<button class="btn btn-primary float-right">Submit</button>
					</div>
				</div>
				<div class="col-md-3"> </div>
			</div>
		</form>
	</div>
</body>

</html>