<?php

namespace Custome\Auth\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\user;
use Illuminate\Support\Facades\Validator;
Use Redirect;
use Session;
use Illuminate\Support\Facades\Hash;
class CustomeAuth extends Controller
{
    /** This  index function is show the Register page view */
    public function index()
    {
        return view('register::register');
    }
    /** This submit function is used to submit register form data  */
    public function submit(Request $request)
    {
        $data = $request->all();
        /** Validation check the all fields is fill or not */
        $validator = Validator::make($data, [
            'name'          => 'required|string',
            'email'         => 'required|email|unique:users',
            'password'      => 'required|min:6',
        ]);
        /** Validation is fails (Fill data is incorrect) then redirect register page  */
        if ($validator->fails()){
            return Redirect::to('register')->withErrors($validator); 
        }
        /** Validation is true then Save your data in database. Create a new user */
        $UserRegister =  User::insert(
            ['name' => $data['name'],
             'email' => $data['email'],
             'password' => Hash::make($data['password'])
            ]
        );
        if($UserRegister){
            Session::flash('success', 'Your registration successfully done.');
          return redirect('login');
        }
    
        
         
    }
    /** This function is show the login page view. */
    public function login()
    {
         return view('login::login');
    }
    /** This function is submit login form data. */
    public function SubmitLogin(request $request) {
        $data = $request->only( 'email', 'password');

           /** Validation check the all fields is fill or not */
          $validator = Validator::make($data, [
              'email'         => 'required',
              'password'      => 'required',
          ]);

          /** Validation is fails (Fill data is incorrect) then redirect login page  */
         if ($validator->fails()) {
              return Redirect::back()->withErrors($validator);
          }
          $email      = $request->post('email');
          $password   = $request->post('password');
          $result     = User::where(['email'=>$email])->first();
        
          if($result){
              if(Hash::check($request->post('password'),$result->password))  {
                  $request->session()->put('user_data',$result);
                 
                    Session::flash('success', 'Your Login successfully done.');
                    return redirect('dashboard');
              
                  
               } else {
                 return Redirect::back()->withErrors('Password is incorrect');
              } 
          }   else {
              return Redirect::back()->withErrors('Invalid username password');
          }
      }

      public function logout(Request $request)
      {
         session()->forget('user_data');
         session()->forget('user_data');
          return redirect('login');
      }
         
}
