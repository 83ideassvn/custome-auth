<?php 
 namespace Custome\Auth;
 use Illuminate\Support\ServiceProvider;

 class CustomeAuthServiceProvider extends ServiceProvider
 {
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');  
        $this->loadviewsFrom(__DIR__ . '/../views','register');  
        $this->loadviewsFrom(__DIR__ . '/../views','login');  
        $this->publishes([
            __DIR__.'/../views' => base_path('resources/views/CustomeAuth'),
        ]);
    }
    /**
     * Register the service provider.
    
     */
    public function register()
    {
        
    }
 }
?>